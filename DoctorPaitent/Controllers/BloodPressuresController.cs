﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DoctorPaitent.Data;
using DoctorPaitent.Models;
using Microsoft.AspNetCore.Authorization;

namespace DoctorPaitent.Controllers
{
    [Authorize]
    public class BloodPressuresController : Controller
    {
        private readonly AuthDbContext _context;

        public BloodPressuresController(AuthDbContext context)
        {
            _context = context;
        }

        // GET: BloodPressures
        public async Task<IActionResult> Index()
        {
            var authDbContext = _context.BloodPressures.Include(b => b.Profile);
            return View(await authDbContext.ToListAsync());
        }

        // GET: BloodPressures/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bloodPressure = await _context.BloodPressures
                .Include(b => b.Profile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bloodPressure == null)
            {
                return NotFound();
            }

            return View(bloodPressure);
        }

        // GET: BloodPressures/Create
        public IActionResult Create()
        {
            ViewData["ProfileId"] = new SelectList(_context.PaitentProfiles, "Id", "Id");
            return View();
        }

        // POST: BloodPressures/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProfileId,Bp")] BloodPressure bloodPressure)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bloodPressure);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProfileId"] = new SelectList(_context.PaitentProfiles, "Id", "Id", bloodPressure.ProfileId);
            return View(bloodPressure);
        }

        // GET: BloodPressures/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bloodPressure = await _context.BloodPressures.FindAsync(id);
            if (bloodPressure == null)
            {
                return NotFound();
            }
            ViewData["ProfileId"] = new SelectList(_context.PaitentProfiles, "Id", "Id", bloodPressure.ProfileId);
            return View(bloodPressure);
        }

        // POST: BloodPressures/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ProfileId,Bp")] BloodPressure bloodPressure)
        {
            if (id != bloodPressure.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bloodPressure);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BloodPressureExists(bloodPressure.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProfileId"] = new SelectList(_context.PaitentProfiles, "Id", "Id", bloodPressure.ProfileId);
            return View(bloodPressure);
        }

        // GET: BloodPressures/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bloodPressure = await _context.BloodPressures
                .Include(b => b.Profile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bloodPressure == null)
            {
                return NotFound();
            }

            return View(bloodPressure);
        }

        // POST: BloodPressures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bloodPressure = await _context.BloodPressures.FindAsync(id);
            _context.BloodPressures.Remove(bloodPressure);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BloodPressureExists(int id)
        {
            return _context.BloodPressures.Any(e => e.Id == id);
        }
    }
}
