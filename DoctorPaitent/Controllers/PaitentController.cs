﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DoctorPaitent.Data;
using DoctorPaitent.Models;

namespace DoctorPaitent.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaitentController : ControllerBase
    {
        private readonly AuthDbContext _context;

        public PaitentController(AuthDbContext context)
        {
            _context = context;
        }

        // GET: api/Paitent
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PaitentProfile>>> GetPaitentProfiles()
        {
            return await _context.PaitentProfiles.Include(i => i.BloodPressures).ToListAsync();
        }

        // GET: api/Paitent/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PaitentProfile>> GetPaitentProfile(int id)
        {
            var paitentProfile = await _context.PaitentProfiles.Include(i=> i.BloodPressures).FirstOrDefaultAsync(i=> i.Id==id);

            if (paitentProfile == null)
            {
                return NotFound();
            }

            return paitentProfile;
        }

        // PUT: api/Paitent/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPaitentProfile(int id, PaitentProfile paitentProfile)
        {
            if (id != paitentProfile.Id)
            {
                return BadRequest();
            }

            _context.Entry(paitentProfile).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaitentProfileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Paitent
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<PaitentProfile>> PostPaitentProfile(PaitentProfile paitentProfile)
        {
            _context.PaitentProfiles.Add(paitentProfile);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPaitentProfile", new { id = paitentProfile.Id }, paitentProfile);
        }

        // DELETE: api/Paitent/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PaitentProfile>> DeletePaitentProfile(int id)
        {
            var paitentProfile = await _context.PaitentProfiles.FindAsync(id);
            if (paitentProfile == null)
            {
                return NotFound();
            }

            _context.PaitentProfiles.Remove(paitentProfile);
            await _context.SaveChangesAsync();

            return paitentProfile;
        }

        private bool PaitentProfileExists(int id)
        {
            return _context.PaitentProfiles.Any(e => e.Id == id);
        }
    }
}
