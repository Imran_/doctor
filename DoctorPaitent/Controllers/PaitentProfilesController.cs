﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DoctorPaitent.Data;
using DoctorPaitent.Models;
using Microsoft.AspNetCore.Authorization;

namespace DoctorPaitent.Controllers
{
    [Authorize]
    public class PaitentProfilesController : Controller
    {
        private readonly AuthDbContext _context;

        public PaitentProfilesController(AuthDbContext context)
        {
            _context = context;
        }

        // GET: PaitentProfiles
        public async Task<IActionResult> Index()
        {
            return View(await _context.PaitentProfiles.ToListAsync());
        }

        // GET: PaitentProfiles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paitentProfile = await _context.PaitentProfiles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (paitentProfile == null)
            {
                return NotFound();
            }

            return View(paitentProfile);
        }

        // GET: PaitentProfiles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PaitentProfiles/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Rating,StarRating,StoryLine,Online,ImageUrl")] PaitentProfile paitentProfile)
        {
            if (ModelState.IsValid)
            {
                _context.Add(paitentProfile);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(paitentProfile);
        }

        // GET: PaitentProfiles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paitentProfile = await _context.PaitentProfiles.FindAsync(id);
            if (paitentProfile == null)
            {
                return NotFound();
            }
            return View(paitentProfile);
        }

        // POST: PaitentProfiles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Rating,StarRating,StoryLine,Online,ImageUrl")] PaitentProfile paitentProfile)
        {
            if (id != paitentProfile.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(paitentProfile);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PaitentProfileExists(paitentProfile.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(paitentProfile);
        }

        // GET: PaitentProfiles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paitentProfile = await _context.PaitentProfiles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (paitentProfile == null)
            {
                return NotFound();
            }

            return View(paitentProfile);
        }

        // POST: PaitentProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var paitentProfile = await _context.PaitentProfiles.FindAsync(id);
            _context.PaitentProfiles.Remove(paitentProfile);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PaitentProfileExists(int id)
        {
            return _context.PaitentProfiles.Any(e => e.Id == id);
        }
    }
}
