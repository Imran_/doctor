﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using DoctorPaitent.Data;
using DoctorPaitent.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

using Microsoft.IdentityModel.Tokens;

namespace DoctorPaitent.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class TokenController : Controller
    {
        readonly UserManager<AppUser> userManager;
        readonly RoleManager<AppRole> roleManager;
        readonly SignInManager<AppUser> signInManager;
       
        private readonly AuthDbContext context;
        
        public TokenController(
           UserManager<AppUser> userManager,
           SignInManager<AppUser> signInManager,
           AuthDbContext context,
           RoleManager<AppRole> roleManager

          )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.context = context;
            this.roleManager = roleManager;
            
        }


        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> CreateToken([FromBody] LoginViewModel loginModel)
        {
            if (ModelState.IsValid)
            {
                var loginResult = await signInManager.PasswordSignInAsync(loginModel.Username, loginModel.Password, isPersistent: false, lockoutOnFailure: false);

                if (!loginResult.Succeeded)
                {
                    return BadRequest();
                }

                var user = await userManager.FindByNameAsync(loginModel.Username);
                var roles = await userManager.GetRolesAsync(user);
                return Ok(GetToken(user, roles.ToList()));
            }
            return BadRequest(ModelState);

        }

        [Authorize]
        [HttpPost]
        [Route("refreshtoken")]
        public async Task<IActionResult> RefreshToken()
        {
            var user = await userManager.FindByNameAsync(
                User.Identity.Name ??
                User.Claims.Where(c => c.Properties.ContainsKey("unique_name")).Select(c => c.Value).FirstOrDefault()
                );
            var roles = await userManager.GetRolesAsync(user);
            return Ok(GetToken(user,roles.ToList()));

        }


        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel registerModel)
        {
            if (ModelState.IsValid)
            {
                var user = new AppUser
                {
                    //TODO: Use Automapper instaed of manual binding  

                    UserName = registerModel.Username,
                    FirstName = registerModel.FirstName,
                    LastName = registerModel.LastName,
                    Email = registerModel.Email
                };

                var identityResult = await this.userManager.CreateAsync(user, registerModel.Password);

                
                if (identityResult.Succeeded)
                {
                    if (!await roleManager.RoleExistsAsync("doctor"))
                       await roleManager.CreateAsync(new AppRole { Id = Guid.NewGuid().ToString(), Name = "doctor", NormalizedName = "DOCTOR" });
                    if (!await roleManager.RoleExistsAsync("patient"))
                        await roleManager.CreateAsync(new AppRole { Id = Guid.NewGuid().ToString(), Name = "patient", NormalizedName = "PATIENT" });
                    if (registerModel.Role == "doctor")
                        await userManager.AddToRoleAsync(user, "doctor");
                    else
                        await userManager.AddToRoleAsync(user, "patient");
                    await signInManager.SignInAsync(user, isPersistent: false);
                    var roles = await userManager.GetRolesAsync(user);
                    return Ok(GetToken(user,roles.ToList()));
                }
                else
                {
                    return BadRequest(identityResult.Errors);
                }
            }
            return BadRequest(ModelState);


        }


       

       
        private object GetToken(AppUser user,List<string> roles)
        {
            var utcNow = DateTime.UtcNow;

            var claims = new Claim[]
            {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, utcNow.ToString()),
                        new Claim("roles",string.Join(",",roles))
            };

            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("fiversecretqazwsxedcrfv"));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            var jwt = new JwtSecurityToken(
                signingCredentials: signingCredentials,
                claims: claims,
                notBefore: utcNow,
                expires: utcNow.AddYears(1),
                audience: "Fiver.Security.Bearer",
                issuer: "Fiver.Security.Bearer"
                );

            return new { access_token = new JwtSecurityTokenHandler().WriteToken(jwt), token_type = "Bearer", expires_in = jwt.ValidTo.ToFileTimeUtc(), userName = user.UserName, _issued = jwt.ValidFrom, _expires = jwt.ValidTo, firstName = user.FirstName, lastName = user.LastName, profilePicture = user.ProfilePicture };

        }


        //[HttpPost]
        //[Route("OTP")]
        //public async Task<IActionResult> OTP([FromBody] OTPRequestModel loginModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (!string.IsNullOrEmpty(loginModel.PartyId))
        //        {
        //            var people = context.
        //                Events.
        //                Include(i => i.People).
        //                Where(i => i.Code == loginModel.PartyId).
        //                SelectMany(i => i.People).
        //                Distinct().
        //                Any(i => i.MobileNo == loginModel.PhoneNumber);
        //            if (people)
        //            {
        //                var random = new Random();
        //                var OTP = new OneTimePassword
        //                {
        //                    PhoneNumber = loginModel.PhoneNumber,
        //                    OnDateTime = DateTime.UtcNow,
        //                    OTP = random.Next(1000, 9999).ToString(),
        //                    Party = loginModel.PartyId
        //                };
        //                context.OneTimePasswords.Add(OTP);
        //                context.SaveChanges();

        //                await Sms.SendMessage(OTP.PhoneNumber, "Login OTP:" + OTP.OTP);
        //                return Ok(OTP);
        //            }
        //            ModelState.AddModelError("PartyId", "Wrong Party Id / Phone Number");
        //        }
        //        else
        //        {
        //            var People = context
        //                .InvitedUsers
        //                .Any(i => i.PhoneNumber == loginModel.PhoneNumber);
        //            var PeopleList = context.People.Any(i => i.MobileNo == loginModel.PhoneNumber);
        //            if (People || PeopleList)
        //            {
        //                var random = new Random();
        //                var OTP = new OneTimePassword
        //                {
        //                    PhoneNumber = loginModel.PhoneNumber,
        //                    OnDateTime = DateTime.UtcNow,
        //                    OTP = random.Next(1000, 9999).ToString(),
        //                    Party = "No Party"
        //                };
        //                context.OneTimePasswords.Add(OTP);
        //                context.SaveChanges();
        //                await Sms.SendMessage(OTP.PhoneNumber, "Login OTP:" + OTP.OTP);
        //                return Ok(OTP);
        //            }
        //            ModelState.AddModelError("PhoneNumber", "No such Number Found in System");
        //        }

        //    }
        //    return BadRequest(ModelState);

        //}

        //[HttpPost]
        //[Route("OTPVerify")]
        //public async Task<IActionResult> OTPVerify([FromBody] VerifyOTPModel loginModel)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var people = context.
        //            OneTimePasswords.
        //            Any(i => i.PhoneNumber == loginModel.PhoneNumber && i.OTP == loginModel.OTP && !i.Expired);
        //        if (people)
        //        {
        //            var par = context.
        //                InvitedUsers.
        //                FirstOrDefault(i => i.PhoneNumber == loginModel.PhoneNumber);
        //            if (par == null)
        //            {
        //                par = new InvitedUser
        //                {
        //                    PhoneNumber = loginModel.PhoneNumber,
        //                    City = "City",
        //                    Name = loginModel.PhoneNumber,
        //                    Points = 0,
        //                    ProfilePic = "https://cdn2.iconfinder.com/data/icons/lucid-generic/24/User_person_avtar_profile_picture_dp-512.png"
        //                };
        //                context.InvitedUsers.Add(par);
        //                await context.SaveChangesAsync();
        //            }
        //            var otp = context
        //                .OneTimePasswords
        //                .FirstOrDefault(i => i.PhoneNumber == loginModel.PhoneNumber && i.OTP == loginModel.OTP);

        //            context.OneTimePasswords.Remove(otp);
        //            context.SaveChanges();
        //            return Ok(GetToken2(par));
        //        }
        //        else
        //        {
        //            var expiredOtp = context.OneTimePasswords.Where(i => i.Expired);
        //            context.OneTimePasswords.RemoveRange(expiredOtp);
        //            await context.SaveChangesAsync();
        //        }
        //        ModelState.AddModelError("OTP", "Wrong OTP or Expired OTP");
        //    }
        //    return BadRequest(ModelState);

        //}

       
    }

}
