﻿using System;
using System.Collections.Generic;
using System.Text;
using DoctorPaitent.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DoctorPaitent.Data
{
    public class AuthDbContext : IdentityDbContext<AppUser, AppRole, string>
    {
        public AuthDbContext(DbContextOptions<AuthDbContext> options)
            : base(options)
        {
        }

        public DbSet<PaitentProfile> PaitentProfiles { get; set; }
        public DbSet<BloodPressure> BloodPressures { get; set; }
    }

    public class AppUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicture { get; set; }
        public bool IsOnline { get; set; }
        public string ConnectionId { get; set; }
       
    }
    public class AppRole : IdentityRole
    {

    }
}
