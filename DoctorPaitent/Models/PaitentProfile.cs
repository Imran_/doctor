﻿using DoctorPaitent.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace DoctorPaitent.Models
{
    public class PaitentProfile
    {
      
        public int Id { get; set; }
        //public string UserId { get; set; }
        //public virtual AppUser User { get; set; }
        public string Name { get; set; }
        public double Rating { get; set; }
        public double StarRating { get; set; }
        public virtual ICollection<BloodPressure> BloodPressures { get; set; }
        public List<string> BP => BloodPressures != null ? BloodPressures.Select(i => i.Bp).ToList() : new List<string>();
        public string StoryLine { get; set; }
        public bool Online { get; set; }
        public string ImageUrl { get; set; }
    }

    public class BloodPressure
    {
       
        public int Id { get; set; }

        public int ProfileId { get; set; }
        public virtual PaitentProfile Profile { get; set; }
        public string Bp { get; set; }
    }

    public class DoctorProfile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Bio { get; set; }
        public string BannerUrl { get; set; }
        public string SignUrl { get; set; }
    }
}
/*
 * 
 * 
 * this.imageUrl,  
    this.name,
    this.rating,
    this.starRating,
    this.bp,
    this.storyline,
    this.online
*/