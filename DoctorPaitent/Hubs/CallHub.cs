﻿using DoctorPaitent.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DoctorPaitent.Hubs
{
    [Authorize]
    public class CallHub:Hub
    {
        readonly private AuthDbContext context;
        public CallHub(AuthDbContext authDbContext)
        {
            context = authDbContext;
        }
        public override Task OnConnectedAsync()
        {
            var UserId = Context.User.Identity.Name;


            return base.OnConnectedAsync();
        }
    }
}
